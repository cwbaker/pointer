
#include <unit/UnitTest.h>
#include <unit/TestReporterStdout.h>

int main( int /*argc*/, char** /*argv*/ )
{
    return UnitTest::RunAllTests();
}
